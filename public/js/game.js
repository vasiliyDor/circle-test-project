(function () {

	var xhr;
	const FIRST_PLAYER_COLOR = '#0000FF';  // Blue
	const SECOND_PLAYER_COLOR = '#FF0000'; // Red
	const INACTIVE_CELL_COLOR = '#FFFFFF';
	const ROW_NUM = 3;
	const COL_NUM = 3;
	const DELAY = 1000;
	const RAND_STRING_LENGTH = 10;

	// We use this variable in case we want to stop observing
	var observer;

	// We want to store some elements and name them starting with "_"
	var _cells = [], _svg, _clearButton, _notices, _info;

	// This variable shows if the game is started
	var isGameStarted = false;

	// This variable shows the color of current player
	var playerColor;

	// This variable shows whose turn is now, blue player goes first
	var whoseTurn = 'blue';

	// This variable temporarily prevents user from clicking a cell
	// It prevents the user from quickly clear the cells and start the game
	var click_blocked = false;

	// Player ID, kind of session token
	var playerID;

	var areTwoPlayersInTheGame = false;

	var canThisPlayerPlay = false;

	// Text constants
	const START_THE_GAME = 'Start the game!';
	const WAIT_PLAYER = 'Please, wait for the other player';
	const EMPTY_CELL = 'Sorry, you may choose an empty cell only';
	const YOUR_TURN = "Now is your turn";
	const OPPONENT_TURN = "Now is your opponent's turn";
	const COLOR_BLUE = "Your color is <span class='blue'>Blue</span>";
	const COLOR_RED = "Your color is <span class='red'>Red</span>";
	const WATCHER = 'You can only watch the game. Wait until players stop the game or clear this game';


	// click event handler for game board cells
	function cellOnClick() {
		// In case 2 players in the game , the third has to watch
		if (!canThisPlayerPlay){
			return false;
		}

		if (click_blocked) {
			_notices.innerHTML = WAIT_PLAYER;
			return false;
		}

		// We don't allow to click on cell which is already clicked
		var isCellEmpty = this.getAttribute( 'fill' ) == INACTIVE_CELL_COLOR;
		if (!isCellEmpty) {
			_notices.innerHTML = EMPTY_CELL;
			return false;
		}

		// We want players to follow the order
		var red_violates = (whoseTurn == 'blue') && (playerColor == SECOND_PLAYER_COLOR);
		var blue_violates = (whoseTurn == 'red') && (playerColor == FIRST_PLAYER_COLOR);
		if (red_violates || blue_violates) {
			return false;
		}

		// we don't want player to click twice
		click_blocked = true;

		// We want to assign playerColor only once
		if (playerColor == undefined) {
			if (isGameStarted) {
				startGameAsRed(); //start the game as a second player
			} else {
				startGameAsBlue();
			}
		}

		var data = {
			// String to check SQL injection
			// 'row': parseInt(this.id[4]) + " OR row_num > -1 -- ",
			'row': parseInt( this.id[4] ),
			'col': parseInt( this.id[5] ),
			'value': playerColor == FIRST_PLAYER_COLOR ? 1 : 2
		};

		var xhr = new XMLHttpRequest();

		// callback for responses
		xhr.onreadystatechange = function () {
			var response, cell;

			// if the state is complete
			if (xhr.readyState === 4) {
				// if the request succeeded in HTTP terms
				if (xhr.status === 200) {
					// parse the response

					response = JSON.parse( xhr.responseText );

					// if the response indicates success, change the cell appearance
					if (response['set'] === true) {
						cell = _svg.getElementById(
							'cell' + response['row'].toString() + response['col'].toString()
						);

						if (cell !== null) {
							if (response['value'] === 1) {
								cell.setAttribute( 'fill', FIRST_PLAYER_COLOR );
							}
							else if (response['value'] === 2) {
								cell.setAttribute( 'fill', SECOND_PLAYER_COLOR );
							}
						}
						if (canThisPlayerPlay){
							_notices.innerHTML = OPPONENT_TURN;
						}

					}
				}
				else {
					console.log( 'There was an error interacting with the game service.' );
				}
			}
		};

		// prep ajax request
		xhr.open( 'PUT', 'game/', true );
		xhr.setRequestHeader( 'Content-Type', 'text/json' );

		// send ajax request
		xhr.send( JSON.stringify( data ) );
	}

	// init function
	function initializeGameBoard() {

		cacheElements();

		bindEvents();


		// Getting the game state initially ...
		getTheGameState().then(function(res){
			// If there is no players we allow to start the game
			if (!areTwoPlayersInTheGame){
				canThisPlayerPlay = true;
			} else { // Check if the user is one of the players
				isPlayerActive().then( function (res) {
					if (res == 'blue'){
						playerColor = FIRST_PLAYER_COLOR;
						_info.innerHTML = COLOR_BLUE;
						canThisPlayerPlay = true;
					}
					else if (res == 'red'){
						playerColor = SECOND_PLAYER_COLOR;
						_info.innerHTML = COLOR_RED;
						canThisPlayerPlay = true;
					}
					else if (res == false){
						if (areTwoPlayersInTheGame){
							_notices.innerHTML = WATCHER;
						}
					}

				} );
			}

		});

		// ... and run the observer (we use variable in case we want to stop it)
		observer = setInterval( function () {
			getTheGameState();
		}, DELAY );
	}


	// Getting the game state
	function getTheGameState() {
		return new Promise( function (resolve, reject) {
			xhr = new XMLHttpRequest();

			// prep ajax request
			xhr.open( 'GET', 'game/', true );
			xhr.setRequestHeader( 'Content-Type', 'text/json' );

			// send ajax request
			xhr.send();

			// callback for responses
			xhr.onreadystatechange = function () {
				// if the state is complete
				if (xhr.readyState === 4) {
					// if the request succeeded in HTTP terms
					if (xhr.status === 200) {
						// parse the response
						var response = JSON.parse( xhr.responseText );

						if (response.result == 'success') {
							var cells = response.cells;

							// set the turn
							whoseTurn = response.turn;

							// reset the variable, we will reassign it if the game has started
							isGameStarted = false;

							var blue_started = false;
							var red_started = false;
							// Fill the color for each cell which has 1 as a value
							cells.forEach( function (cell) {
								var current_cell = _svg.getElementById(
									'cell' + cell['row_num'].toString() + cell['col_num'].toString()
								);
								if (current_cell !== null) {
									if (cell.value == 1) {
										current_cell.setAttribute( 'fill', FIRST_PLAYER_COLOR );
										isGameStarted = true;
										blue_started = true;
									}
									else if (cell.value == 2) {
										current_cell.setAttribute( 'fill', SECOND_PLAYER_COLOR );
										red_started = true;
									}
									else if (cell.value == 0) {
										current_cell.setAttribute( 'fill', INACTIVE_CELL_COLOR );
									}
								}
							} );
							if (blue_started && red_started) {
								areTwoPlayersInTheGame = true;
							}

							// If we have a winner
							if (response.winner != false){
								makeWinner(response.winner);
							}

							// if the game is finished by other player we want to unset player color
							// and start the new game
							if (!isGameStarted) {
								clearApp();
							} else {
								if (canThisPlayerPlay){
									// We want to display whose turn is now
									if (isCurrentPlayerTurn()) {
										_notices.innerHTML = YOUR_TURN;
										click_blocked = false;
									} else {
										_notices.innerHTML = OPPONENT_TURN;
									}
								}

							}
							resolve (true);

						}
						else if (response.result == 'error') {
							console.log( response.message );
						}
					}
					else {
						console.log( 'There was an error loading the cells state.' );
					}
				}
			};
		});
	}


	// Determines if it is current player's turn now
	function isCurrentPlayerTurn() {
		if ((whoseTurn == 'blue') && (playerColor == FIRST_PLAYER_COLOR)) {
			return true;
		}

		if ((whoseTurn == 'red') && (playerColor == SECOND_PLAYER_COLOR)) {
			return true;
		}

		// If we are the second player but still have not took a turn
		if ((whoseTurn == 'red') && (playerColor == undefined)) {
			return true;
		}

		// returns if only none of the above worked
		return false;
	}


	// Caching some page elements
	function cacheElements() {
		// SVG element
		_svg = document.getElementById( 'gameboard' ).contentDocument;

		// Cells
		for (var row = 0; row < ROW_NUM; row++) {
			for (var col = 0; col < COL_NUM; col++) {
				var cell = _svg.getElementById( 'cell' + row.toString() + col.toString() );
				_cells.push( cell );
			}
		}

		// Clear button
		_clearButton = document.getElementById( 'clearButton' );

		// Notices container. Information can be changed in these container while playing
		_notices = document.getElementById( 'notices' );

		// Info containter. Information can not be changed in these container while playing
		_info = document.getElementById( 'info' );
	}


	// Binding the page elements' events
	function bindEvents() {

		// Cell click events
		_cells.forEach( function (cell) {
			cell.addEventListener( 'click', cellOnClick );
		} );

		// Clear button event
		_clearButton.addEventListener( 'click', clearGame );
	}

	// start game - set the players color etc.
	function startGameAsBlue() {
		playerColor = FIRST_PLAYER_COLOR;
		_info.innerHTML = COLOR_BLUE;

		// Getting the uniq ID, kind of session token to set this player as the first
		playerID = getRandomString();
		localStorage.setItem( 'playerID', playerID );
		sendUserId( 'blue' );
	}

	// start the game as a second player
	function startGameAsRed() {
		playerColor = SECOND_PLAYER_COLOR;
		_info.innerHTML = COLOR_RED;

		// Getting the uniq ID, kind of session token to set this player as the first
		playerID = getRandomString();
		localStorage.setItem( 'playerID', playerID );
		sendUserId( 'red' );
	}

	// check if we are the players
	function isPlayerActive() {
		click_blocked = true;
		var id = localStorage.getItem( 'playerID' );
		if (!id) {
			return new Promise( function (resolve, reject){
				resolve (false);
			});
		} else if (id != "") {
			return checkUserState( id ).then( function (result) {
				return result
			} )
		}
	}

	function checkUserState(player_id) {
		return new Promise( function (resolve, reject) {
			var xhr = new XMLHttpRequest();

			// prep ajax request
			xhr.open( 'GET', 'user/?player_id=' + player_id, true );
			xhr.setRequestHeader( 'Content-Type', 'text/json' );

			// send ajax request
			xhr.send();

			// callback for responses
			xhr.onreadystatechange = function () {
				// if the state is complete
				if (xhr.readyState === 4) {
					// if the request succeeded in HTTP terms
					if (xhr.status === 200) {
						// parse the response
						var response = JSON.parse( xhr.responseText );

						if (response.result == 'success') {
							resolve( response.user.color );
						}
						else if (response.result == 'error') {
							resolve( false );
							console.log( response.message );
						}
					}
					else {
						console.log( 'There was an error creating an user.' );
					}
				}
			};
		} );
	}

	// Send user's ID to server to store user's color
	function sendUserId(color) {
		var data = {
			'player_id': playerID,
			'color': color
		};

		xhr = new XMLHttpRequest();

		// prep ajax request
		xhr.open( 'POST', 'user/', true );
		xhr.setRequestHeader( 'Content-Type', 'text/json' );

		// send ajax request
		xhr.send( JSON.stringify( data ) );

		// callback for responses
		xhr.onreadystatechange = function () {
			// if the state is complete

			if (xhr.readyState === 4) {
				// if the request succeeded in HTTP terms
				if (xhr.status === 200) {
					// parse the response
					var response = JSON.parse( xhr.responseText );

					if (response.result == 'success') {
						// Do something in case we need
					}
					else if (response.result == 'error') {
						console.log( response.message );
					}
				}
				else {
					console.log( 'There was an error creating an user.' );
				}
			}
		};
	}


	function getRandomString() {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for (var i = 0; i < RAND_STRING_LENGTH; i++)
			text += possible.charAt( Math.floor( Math.random() * possible.length ) );

		return text;
	}


	// Clearing the cells
	function clearGame() {
		// It removes the bug: user could start the new game quickly, so other player
		// may not see the change
		click_blocked = true;
		setTimeout( function () {
			click_blocked = false;
		}, DELAY );

		// Firstly, clear the cells state
		var xhr = new XMLHttpRequest();

		// prep ajax request
		xhr.open( 'DELETE', 'game/', true );
		xhr.setRequestHeader( 'Content-Type', 'text/json' );

		// send ajax request
		xhr.send();

		// callback for responses
		xhr.onreadystatechange = function () {
			// if the state is complete
			if (xhr.readyState === 4) {
				// if the request succeeded in HTTP terms

				if (xhr.status === 200) {
					// parse the response
					var response = JSON.parse( xhr.responseText );

					if (response.result == 'success') {
						// Fill the color for each cell which has 1 as a value
						_cells.forEach( function (cell) {
							cell.setAttribute( 'fill', INACTIVE_CELL_COLOR );
						} );

						clearApp();
					}
					else if (response.result == 'error') {
						console.log( response.message );
					}
				}
				else {
					console.log( 'There was an error clearing the cells state.' );
				}
			}
		};


		// Then delete users
		var user_xhr = new XMLHttpRequest();

		// prep ajax request
		user_xhr.open( 'DELETE', 'user/', true );
		user_xhr.setRequestHeader( 'Content-Type', 'text/json' );

		// send ajax request
		user_xhr.send();

		// callback for responses
		user_xhr.onreadystatechange = function () {
			// if the state is complete
			if (user_xhr.readyState === 4) {
				// if the request succeeded in HTTP terms
				if (user_xhr.status === 200) {
					// parse the response
					var response = JSON.parse( xhr.responseText );

					if (response.result == 'success') {
						console.log( "Users deleted" );
					}
					else if (response.result == 'error') {
						console.log( response.message );
					}
				}
				else {
					console.log( 'There was an error deleting the users.' );
				}
			}
		}
	}

	// we want to start game again so we want to erase the information about who is first
	function clearApp(){
		playerColor = undefined;
		isGameStarted = false;
		localStorage.setItem( "playerID", "" );
		_notices.innerHTML = START_THE_GAME;
		_info.innerHTML = '';
		click_blocked = false;
		canThisPlayerPlay = true;

	}

	function makeWinner(color){
		canThisPlayerPlay = false;
		click_blocked = true
		if (color == 'red'){
			_notices.innerHTML = "Red player is a winner!";
		} else {
			_notices.innerHTML = "Blue player is a winner!";
		}
	}

	window.onload = initializeGameBoard;

})();