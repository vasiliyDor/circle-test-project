﻿# Web Developer Programming Challenge

(c) 2016 Circle Cardiovascular Imaging Inc.

This source code is a very basic RESTful Web Service and HTML5/JavaScript application that happens
to work much like the game of tic-tac-toe. Using this sample code as a template, you will add new
features and make corrections to create a more fully-featured tic-tac-toe game with persistence and
multi-player abilities.

An SVG game board is embedded as an object and connected to callbacks that trigger AJAX calls to a
back-end web service. The web service is extremely basic, but receives and returns JSON in a RESTful
manner and operates with an SQLite database for persistence.


## Rules

1. You have 24 hours to complete the challenge.
2. You may *not* use a third-party PHP framework for REST or database access.
3. You may use a third-party JavaScript library such as jQuery, but it is not required.
4. You may use a third-party JavaScript MVC/MVVM framework in-browser, such as Angular, Backbone,
   or Ember, but it is not required.
5. You can assume that the HTML5/Javascript application will run in a modern and recently-updated
   web browser.



## Getting Started

Install PHP 5.5 or above and a web server (optional - see the hint below). PHP doesn't need many
modules, but you must enable at minimum the sqlite and PDO modules.

MacOS X may already have a sufficient version of PHP integrated.

Start a web server, pointing at the public/ directory. **Hint**: If you only install PHP, you can
start a simple web server at http://localhost:8080 from the command line like so:

    C:> cd <code directory>\public\
    C:> php -S localhost:8080


## Challenges

1. In app/GameModel.inc.php, refactor GameModel::bootstrapDatabase() so that the INSERT command is
   defined only once, and the nine rows are inserted using a nested loop. _Hint:_ This is often
   referred to as prepare/bind.

2. In app/GameModel.inc.php, there is a function that contains a common vulnerability. Find the
   vulnerability and fix it.

3. Enhance the Database Definition Language code in GameMode::bootstrapDatabase() so that it would
   not be possible to have two table-rows with the same combination of row_num and col_num.

4. The application stores the fact that a cell has been clicked in the database, but the board is
   reset by page loads. Using AJAX, GET the board state when the page loads and initialize the
   board's cell colours.

5. Enhance the AJAX and web service PUT operations so that clicking a white cell turns it on (green)
   and clicking a green cell turns it off (white).

6. Enhance the AJAX GET so that it refreshes the game board every second, so that an observer in a 
   second web browser sees the changes made in the first web browser.

7. Add an event listener to the button named "clearButton" that executes an AJAX DELETE call to the
   back-end. The DELETE action should reset the database so that no cells are "on".

8. Modify the JavaScript application so that the tic-tac-toe cells can be turned either blue
   (initial default on page load) or red.
    - The cell cannot be turned white except by using the "Clear" button.
    - The current colour is sent via AJAX to the back end with the cell row/column, and the value
      of the cell's table-row in the database should reflect which colour was set (1 for blue, 2
      for red).
    - The AJAX GET operation should include this value correctly and the Javascript app should
      colour the board correctly on all watching browsers.
    - The AJAX PUT updates should return whose turn it is (in terms of colour) and set that state
      in the JavaScript app for the current browser, so that red and blue alternate (take turns).

9. In PHP, modify the AJAX GET handlers to check for a tic-tac-toe winner by using a
   [Magic Square](http://mathworld.wolfram.com/MagicSquare.html). If a winner is found, return that
   information to the JavaScript app, which should notify the user by modifying the innerHTML
   property of the "notices" element.

10. Add a column to the GameBoard table to keep a timestamp. This timestamp should either contain
    the time of the last update or NULL if that cell has been cleared.

11. Modify the GET handler to return the current colour turn, in addition to the current game board.
    Use this information to make all connected browsers aware of whose turn it is, so that the game
    may be played in a multi-player fashion from multiple connected browsers.