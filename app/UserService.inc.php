<?php

require_once(__DIR__ . "/DataBase.inc.php");
require_once(__DIR__ . "/UserModel.inc.php");


class HttpRequest
{
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';

    /// HTTP verb in use (GET|POST|PUT|DELETE)
    public $verb = false;

    /// Parameters passed in. Query params or parsed json/xml.
    public $parameters = false;
}


class UserService
{
    protected $m_model = false;

    // Constructor
    public function __construct()
    {
        $this->m_model = new UserModel();
    }

    public function read($request)
    {
        return $this->m_model->read($request);
    }

    public function create($request)
    {
        if ($this->m_model->set($request) == true){
            return [
                'result' => 'success',
            ];
        }
        else{
            return [
                'result' => 'error',
                'message' => 'Could not set the user'
            ];
        }
    }

    public function delete($request)
    {
        return $this->m_model->clear();
    }
}


?>