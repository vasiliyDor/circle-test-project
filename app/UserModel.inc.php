<?php


class UserModel
{
    protected $m_db;

    const MAX_USERS = 2;

    public function __construct()
    {
        $db = new DB;
        $this->m_db = $db->m_db;
    }

    // Get user's state
    public function read($request)
    {
        $player_id =  $request->parameters['player_id'];
        $query = $this->m_db->prepare(
            "SELECT * FROM Users where player_id=?"
        );
        $query->execute([ $player_id ]);
        if ($user = $query->fetchAll()) {

            return [
                'result' => 'success',
                'user' => $user[0]
            ];
        }
        else {
            return [
                'result' => 'error',
                'message' => 'Could not get the user'
            ];
        }
    }

    // Set a row/column cell value
    public function set($request)
    {

        if ($this->getUsersCount() < static::MAX_USERS){
            $player_id = $request->parameters['player_id'];
            $color = $request->parameters['color'];

            $prepared_query = $this->m_db->prepare(
                "INSERT into Users (player_id, color) values(?, ?);"
            );

            $ret = $prepared_query->execute([$player_id, $color]);
            return ($ret !== false);
        } else {
            return [
                'result' => 'error',
                'message' => 'Could not set the 3rd user'
            ];
        }

    }

    // Get list of all users
    public function getUsersCount()
    {
        $query = $this->m_db->query(
            "SELECT count(*) as count FROM Users"
        );

        if ($users_count = $query->fetchAll()) {
            return $users_count[0]['count'];
        }
        else {
            return [
                'result' => 'error',
                'message' => 'Could not get the cells state'
            ];
        }
    }

    // Sets all cells' value at 0
    public function clear()
    {
        $query = $this->m_db->exec(
            "DELETE FROM Users"
        );

        return [
            'result' => 'success'
        ];
    }
}