<?php

require_once(__DIR__ . "/DataBase.inc.php");
require_once(__DIR__ . "/GameModel.inc.php");



class HttpRequest
{
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';

    /// HTTP verb in use (GET|POST|PUT|DELETE)
    public $verb = false;

    /// Parameters passed in. Query params or parsed json/xml.
    public $parameters = false;
}


class GameService
{
    protected $m_model = false;

    // Constructor
    public function __construct()
    {
        $this->m_model = new GameModel();
    }


    public function create($request)
    {
        return $this->update($request);
    }


    public function read($request)
    {
        return $this->m_model->getAll();
    }


    public function update($request)
    {
        return [
            'row' => $request->parameters['row'],
            'col' => $request->parameters['col'],
            'value' => $request->parameters['value'],
            'set' => $this->m_model->set(
            	$request->parameters['row'],
	            $request->parameters['col'],
	            $request->parameters['value']
            )
        ];
    }


    public function delete($request)
    {
	    return $this->m_model->clear();
    }
}


?>