<?php


class GameModel
{
    protected $m_db;

    // How many rows we have
    const ROW_NUM = 3;

    // How many columns we have
    const COL_NUM = 3;

    public function __construct()
    {
        $db = new DB;
        $this->m_db = $db->m_db;
    }

    // Set a row/column cell value
    public function set($row, $col, $value)
    {
        /*  We use PDO prepared query to make sure we are not SQL-injection vulnerable
         *  We could have used something like
         *  $row = intval($row);
         *  and the same for the value and col to make sure that
         *  strings like "1 OR col_row > 0" are transformed to integer
         *
         *  But this solution is better since we can't change the table row we don't want to
         *  change. Moreover, it is universal, we don't have to use escape functions on strings etc.
         */

        $prepared_query = $this->m_db->prepare(
            "UPDATE GameBoard SET value = ?, timestamp=CURRENT_TIMESTAMP WHERE row_num = ? AND col_num = ? ;"
        );
        $ret = $prepared_query->execute([$value, $row, $col]);

        return ($ret !== false);
    }

    // Get list of all cells
    public function getAll()
    {
        $query = $this->m_db->query(
            "SELECT * FROM GameBoard"
        );

        if ($cells = $query->fetchAll()) {
            // We want to know whose turn is now
            // if amount of 1(blue) is more than 2(red)
            // that means that now is 2(red) turn, otherwise 1(blue)
            $red = $blue = 0;
            foreach ($cells as $cell){
                if ($cell['value'] == 1){
                    $blue++;
                }
                elseif ($cell['value'] == 2){
                    $red++;
                }
            }
            if ($blue > $red){
                $turn = 'red';
            }
            else{
                $turn = 'blue';
            }

            return [
                'result' => 'success',
                'cells' => $cells,
                'turn' => $turn,
                'winner' => $this->getWinner($cells)
            ];
        }
        else {
            return [
                'result' => 'error',
                'message' => 'Could not get the cells state'
            ];
        }
    }


    public function getWinner($cells)
    {
        $arr = [];
        // Reformat array to make it more explicit
        foreach ($cells as $cell){
            $index = $cell['row_num'] . $cell['col_num'];
            $arr[$index] = $cell['value'];
        }

        // Checking rows
        for ($row = 0; $row < static::ROW_NUM; $row++){
            $cur_row = '';
            for ($col = 0; $col < static::COL_NUM; $col++){
                $cur_row .= $arr[$row . $col];
            }
            if ($cur_row == '111') {
                return "blue";
            } else if ($cur_row == '222') {
                return "red";
            }
        }

        // Checking cols
        for ($row = 0; $row < static::ROW_NUM; $row++){
            $cur_col = '';
            for ($col = 0; $col < static::COL_NUM; $col++){
                $cur_col .= $arr[$col . $row];
            }
            if ($cur_col == '111') {
                return "blue";
            } else if ($cur_col == '222') {
                return "red";
            }
        }

        // Checking diagonals
        // Diag criteria is
        // row == cel
        // or
        // row + col = 2
        $cur_diag_1 = '';
        $cur_diag_2 = '';
        for ($row = 0; $row < static::ROW_NUM; $row++){

            for ($col = 0; $col < static::COL_NUM; $col++){
                if ($row == $col){ // 1st diag
                    $cur_diag_1 .= $arr[$row . $col];
                }

                if (($row + $col) == 2){ // 2st diag
                    $cur_diag_2 .= $arr[$row . $col];
                }
            }
        }
        if ($cur_diag_1 == '111') {
            return "blue";
        } else if ($cur_diag_1 == '222') {
            return "red";
        }

        if ($cur_diag_2 == '111') {
            return "blue";
        } else if ($cur_diag_2 == '222') {
            return "red";
        }
        return false;
    }

    // Sets all cells' value at 0
    public function clear()
    {
        $query = $this->m_db->exec(
            "UPDATE GameBoard set value = 0, timestamp = NULL"
        );

        // $query contains the amount of changed rows
        if ($query == static::ROW_NUM * static::COL_NUM ) {
            return [
                'result' => 'success'
            ];
        }
        else {
            return [
                'result' => 'error',
                'message' => 'Could not clear the cells\' values'
            ];
        }
    }
}