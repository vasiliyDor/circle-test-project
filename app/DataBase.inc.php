<?php

class DB {

    public $m_db = false;

    const FILE_NAME = "game.db";

    // How many rows we have
    const ROW_NUM = 3;

    // How many columns we have
    const COL_NUM = 3;

    public function __construct()
    {
        $dbMissing = false;

        // check for an existing database file
        if (!file_exists(__DIR__ . DIRECTORY_SEPARATOR . static::FILE_NAME)){
            $dbMissing = true;
        }

        // create the PDO object to connect to the database
        $this->m_db = new PDO('sqlite:' . __DIR__ . DIRECTORY_SEPARATOR . static::FILE_NAME);
        $this->m_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // if the database file was missing, create the default set of tables and data
        if ($dbMissing){
            $this->bootstrapDatabase();
        }

        $this->m_db->exec('PRAGMA foreign_keys = ON;');
    }

    protected function bootstrapDatabase()
    {
        $this->m_db->exec('PRAGMA encoding = "UTF-16le";');
        $this->m_db->exec('PRAGMA application_id = 342;');
        $this->m_db->exec('PRAGMA foreign_keys = OFF;');

        // create the Users table if it does not exist
        $this->m_db->exec(
            "CREATE TABLE IF NOT EXISTS Users (
                player_id varchar(10) NOT NULL,
                color varchar(4) not NULL,
                UNIQUE (color)
            );"
        );

        // create the GameBoard table if it does not exist
        $this->m_db->exec(
            "CREATE TABLE IF NOT EXISTS GameBoard (
                id INTEGER PRIMARY KEY ASC AUTOINCREMENT NOT NULL,
                row_num INTEGER NOT NULL,
                col_num  INTEGER NOT NULL,
                value INTEGER NULL,
                timestamp DATETIME NULL,
                UNIQUE (row_num, col_num)
            );"
        );


        // clear out the GameBoard table (sanity check in case the boostrapDatabase function is
        // called against an existing database)
        $this->m_db->exec("DELETE FROM GameBoard;");


        // Insert standard game board rows
        // Firstly, we prepare the query...
        $prepared_query = $this->m_db->prepare(
            "INSERT INTO GameBoard (row_num, col_num, value, timestamp) VALUES (?, ?, NULL, NULL);"
        );

        // ...then we bind row and col variables and execute the query
        for ($row = 0; $row < static::ROW_NUM; $row++){
            for ($col = 0; $col < static::ROW_NUM; $col++){
                $prepared_query->execute([$row, $col]);
            }
        }
    }
}